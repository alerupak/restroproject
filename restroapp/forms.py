from django import forms
from .models import*


class MessageForm(forms.ModelForm):
	class Meta:
		model = Message
		fields = '__all__'
		widgets = {
		'name' : forms.TextInput(attrs = {
			'class' : 'form-control',
			'placeholder': 'your name...'
			}),
		'email' : forms.EmailInput(attrs = {
			'class' : 'form-control',
			'placeholder' : 'example@gmail.com'
			}),
		'message' : forms.Textarea(attrs = {
			'class': 'form-control',
			'placeholder' : 'message...'
			}),
		}



class OrderForm(forms.ModelForm):
	class Meta:
		model = Order
		fields = '__all__'
		widgets = {
		'name' : forms.TextInput(attrs = {
			'class': 'form-control',
			'placeholder' : 'your name...'
			}),
		'order_name' : forms.Textarea(attrs = {
			'class' : 'form-control',
			'placeholder' : 'your order with quantity...'
			}),
		'location' : forms.TextInput( attrs = {
			'class' : 'form-control',
			'placeholder' : 'your location...'
			}),
		'phone' : forms.TextInput( attrs = {
			'class' : 'form-control',
			'placeholder' : 'your phone...'
			}),
		'pickup_time' : forms.TextInput(attrs = {
			'class' : 'form-control',
			'placeholder' : 'when you want it to be delivered...'
			}),
		}


class ContactForm(forms.ModelForm):
	class Meta:
		model = Contact 
		fields = '__all__'
		widgets = {
		'address' : forms.TextInput(attrs = {
			'class' : 'form-control'
			}),
		'phone' : forms.TextInput(attrs = {
			'class' : 'form-control'
			}),
		'email' : forms.EmailInput(attrs = {
			'class' : 'form-control'
			}),
		'open_day' : forms.TextInput(attrs = {
			'class' : 'form-control'
			}),
		'open_time' : forms.TextInput(attrs = {
			'class' : 'form-control'
			}),
		}



class AboutForm(forms.ModelForm):
	class Meta:
		model = About 
		fields = '__all__'
		widgets = {
		'title' : forms.TextInput(attrs = {
			'class' : 'form-control'
			}),
		'detail' : forms.Textarea(attrs = {
			'class' : 'form-control'
			}),
		}



class SubAboutForm(forms.ModelForm):
	class Meta:
		model = SubAbout 
		fields = '__all__'
		widgets = {
		'title' : forms.TextInput(attrs = {
			'class' : 'form-control'
			}),
		'detail' : forms.Textarea(attrs = {
			'class' : 'form-control'
			}),
		}



class ChefForm(forms.ModelForm):
	class Meta:
		model = Team
		fields = '__all__'
		widgets = {
		'name' : forms.TextInput(attrs = {
			'class' : 'form-control'
			}),
		'post' : forms.TextInput(attrs = {
			'class' : 'form-control'
			}),
		'image' : forms.FileInput(attrs = {
			'class' : 'form-control'
			}),
		'phone' : forms.TextInput(attrs = {
			'class' : 'form-control'
			}),
		'email' : forms.EmailInput(attrs = {
			'class' : 'form-control'
			}),
		'address' : forms.TextInput(attrs = {
			'class' : 'form-control'
			}),
		'facebook' : forms.TextInput(attrs = {
			'class' : 'form-control'
			}),
		'twitter' : forms.TextInput(attrs = {
			'class' : 'form-control'
			}),
		}



class ReasonToChooseForm(forms.ModelForm):
	class Meta:
		model = ReasonToChoose
		fields = '__all__'
		widgets = {
		'title' : forms.TextInput(attrs = {
			'class' : 'form-control'
			}),
		'few_line' : forms.Textarea(attrs = {
			'class' : 'form-control'
			}),
		'image' : forms.FileInput(attrs = {
			'class' : 'form-control'
			}),
		}


class OurSpecialForm(forms.ModelForm):
	class Meta:
		model = OurSpecial
		fields = '__all__'
		widgets = {
		'food_name' : forms.TextInput(attrs = {
			'class' : 'form-control'
			}),
		'combo' : forms.TextInput(attrs = {
			'class' : 'form-control'
			}),
		'price' : forms.TextInput(attrs = {
			'class' : 'form-control'
			}),
		'image' : forms.FileInput(attrs = {
			'class' : 'form-control'
			}),
		}


class ClientVoiceForm(forms.ModelForm):
	class Meta:
		model = ClientVoice 
		fields = '__all__'
		widgets = {
		'name' : forms.TextInput(attrs = {
			'class' : 'form-control'
			}),
		'image' : forms.FileInput(attrs = {
			'class' : 'form-control'
			}),
		'voice' : forms.Textarea(attrs = {
			'class' : 'form-control'
			}),
		'post_company' : forms.TextInput(attrs = {
			'class' : 'form-control'
			}),
		}


class BannerForm(forms.ModelForm):
	class Meta:
		model = Banner 
		fields = '__all__'
		widgets = {
		'slogon' : forms.TextInput(attrs = {
			'class' : 'form-control'
			}),
		'some_text' : forms.TextInput(attrs = {
			'class' : 'form-control'
			}),
		'image' : forms.FileInput(attrs = {
			'class' : 'form-control'
			}),
		'price' : forms.TextInput(attrs = {
			'class' : 'form-control'
			})
		}


class GalleryForm(forms.ModelForm):
	class Meta:
		model = Gallery 
		fields = '__all__'
		widgets = {
		'title' : forms.TextInput(attrs = {
			'class' : 'form-control'
			}),
		'image' : forms.FileInput(attrs = {
			'class' : 'form-control',
			'multiple' : True
			}),
		}


class FoodMenuForm(forms.ModelForm):
	class Meta:
		model = FoodMenu 
		fields = '__all__'
		widgets = {
		'food_name' : forms.TextInput(attrs = {
			'class' : 'form-control'
			}),
		'price' : forms.TextInput(attrs = {
			'class' : 'form-control'
			}),
		'extra' : forms.TextInput(attrs = {
			'class' : 'form-control'
			}),
		'category' : forms.Select(attrs = {
			'class' : 'form-control',
			
			}),
		}


class FoodCategoryForm(forms.ModelForm):
	class Meta:
		model = FoodCategory 
		fields = '__all__'
		widgets = {
		'food_type' : forms.TextInput(attrs = {
			'class' : 'form-control'
			}),
		}		

	

class LoginForm(forms.Form):
	username = forms.CharField(widget = forms.TextInput(attrs = {
		'placeholder' : 'Username','class' : 'ggg'
		}))
	password = forms.CharField(widget = forms.PasswordInput(attrs = {
		'placeholder' : 'Password','class' : 'ggg'
		}))


class AdminRegisterForm(forms.ModelForm):
	username = forms.CharField(widget = forms.TextInput(attrs = {'class' : 'form-control'}))
	password = forms.CharField(widget = forms.PasswordInput(attrs = {'class' : 'form-control'}))
	confirm_password = forms.CharField(widget = forms.PasswordInput(attrs = {
		'class' : 'form-control'}))

	class Meta:
		model = Admin 
		fields = ['username', 'password', 'confirm_password', 'full_name', 'image', 'email',
					'phone','post','address']
		widgets = {
		'full_name' : forms.TextInput(attrs = {
			'class' : 'form-control'
			}),
		'image' : forms.FileInput(attrs = {
			'class' : 'form-control'
			}),
		'email' : forms.EmailInput(attrs = {
			'class' : 'form-control'
			}),
		'phone' : forms.TextInput(attrs = {
			'class' : 'form-control'
			}),
		'post' : forms.TextInput(attrs = {
			'class' : 'form-control'
			}),
		'address' : forms.TextInput(attrs = {
			'class' : 'form-control'
			})
		}


	def clean_username(self):
		uname = self.cleaned_data['username']
		if User.objects.filter(username = uname).exists():
			raise forms.ValidationError("Oops, username already taken")

		return uname

	def clean_confirm_password(self):
		pword = self.cleaned_data['password']
		c_pword = self.cleaned_data['confirm_password']

		if pword != c_pword:
			raise forms.ValidationError("password does not match!!")

		return c_pword









 
