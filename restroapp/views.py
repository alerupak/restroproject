from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.views.generic import*
from .models import *
from .forms import *
from django.contrib.auth.models import User,Group
from django.urls import reverse_lazy
from django.db.models import Q
from django.contrib.auth.tokens import default_token_generator
from django.core.mail import EmailMessage
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.contrib.sites.shortcuts import get_current_site
from django.http import JsonResponse





# Create your views here.

class BaseMixin(object):
	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['contact'] = Contact.objects.all()
		context['aboutdetail'] = About.objects.first()
		context['subabout'] = SubAbout.objects.first()
		context['teamlist'] = Team.objects.all()
		context['reason_to_choose'] = ReasonToChoose.objects.all()
		context['clientvoice'] = ClientVoice.objects.all()
		context['ourspecial'] = OurSpecial.objects.all()
		context['bannerdetailview'] = Banner.objects.first()
		context['gallerylist'] = Gallery.objects.all().order_by('-id')
		context['food_category'] = FoodCategory.objects.all()

		return context


class AdminBaseMixin(object):
	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['messagelist'] = Message.objects.all().order_by('-id')
		context['admindetail'] = Admin.objects.first()
		context['orderlist'] = Order.objects.all().order_by('-id')
		context['contactlist'] = Contact.objects.all()


		return context


class AdminRequiredMixin(object):
	def dispatch(self,request,*args,**kwargs):
		user = request.user
		if user.is_authenticated and user.groups.filter(name = 'Admin').exists():
			pass
		else:
			return redirect('/admin-login/')

		return super().dispatch(request,*args,*kwargs)




#login
class LoginView(FormView):
	template_name = 'clienttemplates/adminlogin.html'
	form_class = LoginForm
	success_url = reverse_lazy('restroapp:adminhome')

	def form_valid(self, form):
		uname = form.cleaned_data["username"]
		pword = form.cleaned_data["password"]
		usr = authenticate(username=uname, password=pword)

        #self.user = usr
		if usr is not None:
			login(self.request, usr)

		else:
			return render(self.request, self.template_name,
							{
								"error": "Incorrect Username or Password",
								"form": form
							})

		return super().form_valid(form)


#logout
class LogoutView(View):
	def get(self,request):
		logout(request)
		return redirect('/')


#username checkr
class UsernameCheckView(View):
	def get(self, request):
		uname = request.GET.get('username')
		print(uname, '######################')
		if User.objects.filter(username = uname).exists():
			message = "username not available"
			color = "red"

		else:
			message = "username available"
			color = "green"

		return JsonResponse({
			'message' : message,
			'color' : color,
			})


#client view
class ClientHomeView(BaseMixin,TemplateView):
	template_name = 'clienttemplates/clienthome.html'

def HOME(request):
	"#Number of visits to this view, as counted in the session variable."
	num_visits = request.session.get('num_visits', 1)
	request.session['num_visits'] = num_visits + 1
	context = {'num_visits': num_visits}

	return render(request,'adminhome.html',context=context)





#about
class AboutDetailView(BaseMixin,DetailView):
	template_name = 'clienttemplates/about.html'
	model = About
	context_object_name = 'aboutdetail'


#sub about
class SubAboutDetailView(BaseMixin,DetailView):
	template_name = 'clienttemplates/about.html'
	model = SubAbout
	context_object_name = 'subaboutdetail'


#message
class MessageCreateView(BaseMixin,CreateView):
	template_name = 'clienttemplates/contact.html'
	form_class = MessageForm
	success_url = reverse_lazy('restroapp:messagecreate')

#contact
class ContactListView(BaseMixin,ListView):
	template_name = 'clienttemplates/contact.html'
	model = Contact
	context_object_name = 'contact'





#team
class TeamListView(BaseMixin,ListView):
	template_name = 'clienttemplates/about.html'
	model = Team
	context_object_name = 'teamlist'


#reason_to_choose
class ReasonToChooseListView(BaseMixin, ListView):
	template_name = 'clienttemplates/about.html'
	model = ReasonToChoose
	context_object_name = 'reason_to_choose'


#client_voice
class ClientVoiceListView(BaseMixin,ListView):
	template_name = 'clienttemplates/clienthome.html'
	model = ClientVoice
	context_object_name = 'clientvoice'


#order
class OrderCreateView(BaseMixin,CreateView):
	template_name = 'clienttemplates/oder.html'
	form_class = OrderForm
	success_url = reverse_lazy('restroapp:clienthome')



#ourspecial
class OurSpecialListView(ListView):
	template_name = 'clienttemplates/clienthome.html'
	model = OurSpecial 
	context_object_name = 'ourspecial'


#banner
class BannerDetailView(DetailView):
	template_name = 'clienttemplates/clienthome.html'
	model = Banner
	context_object_name = 'bannerdetailview'


#gallery
class GalleryListView(BaseMixin,ListView):
	template_name = 'clienttemplates/gallerylist.html'
	queryset = Gallery.objects.all().order_by('-id')
	context_object_name = 'gallerylistview'




#food menu
class Food_MenuListView(BaseMixin,ListView):
	template_name = 'clienttemplates/menu.html'
	queryset = FoodMenu.objects.all()
	context_object_name = 'food_menulist'


# food category
class FoodCategory_ListView(BaseMixin,ListView):
	template_name = 'clienttemplates/menu.html'
	queryset = FoodCategory.objects.all()
	context_object_name = 'food_categorylist'




#admin views start here
class AdminHomeView(AdminRequiredMixin,AdminBaseMixin,TemplateView):
	template_name = 'admintemplates/adminhome.html'

def Home(request):
	num_visits = request.session.get('num_visits', 0)
	request.session['num_visits'] = num_visits + 1
	context = {'num_visits' : num_visits }

	return context


#adminprofile
class AdminProfileDetailView(AdminRequiredMixin,AdminBaseMixin,TemplateView):
	template_name = 'admintemplates/adminprofiledetail.html'

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		user = self.request.user
		thisuser = Admin.objects.filter(user=user)
		context["admindetail"]=thisuser

		return context



# contact
class A_ContactCreateView(AdminRequiredMixin,CreateView):
	template_name = 'admintemplates/contactupdate.html'
	form_class = ContactForm
	success_url = '/admin/home/'




class AdminContactUpdateView(AdminRequiredMixin,AdminBaseMixin,UpdateView):
	template_name = 'admintemplates/contactupdate.html'
	model = Contact
	form_class = ContactForm
	success_url = reverse_lazy('restroapp:adminhome') 


class A_ContectListView(AdminRequiredMixin,AdminBaseMixin,ListView):
	template_name = 'admintemplates/a_contactlist.html'
	model = Contact 
	context_object_name = 'a_contactlistview'

#message
class MessageListView(AdminRequiredMixin,AdminBaseMixin,ListView):
	template_name = 'admintemplates/messagelist.html'
	queryset = Message.objects.all().order_by('-id')
	context_object_name = 'message'


class MessageDeleteView(AdminBaseMixin,DeleteView):
	template_name = 'admintemplates/messagedelete.html'
	model = Message
	success_url = '/message/list/'



class MessageDetailView(AdminRequiredMixin,AdminBaseMixin,DetailView):
	template_name = 'admintemplates/messagedeteil.html'
	model = Message 
	context_object_name = 'messagedetail'



#about views
class A_AboutListView(AdminRequiredMixin,AdminBaseMixin,ListView):
	template_name = 'admintemplates/a_aboutlist.html'
	model = About 
	context_object_name = 'a_aboutlistview'




class AboutUpdateView(AdminRequiredMixin,AdminBaseMixin,UpdateView):
	template_name = 'admintemplates/aboutupdate.html'
	model = About 
	form_class = AboutForm
	success_url = reverse_lazy('restroapp:adminhome')


class A_SubAboutListView(AdminRequiredMixin,AdminBaseMixin,ListView):
	template_name = 'admintemplates/a_subaboutlist.html'
	model = SubAbout
	context_object_name = 'a_subaboutlistview'


class SubAboutUpdateView(AdminRequiredMixin,AdminBaseMixin,UpdateView):
	template_name = 'admintemplates/subaboutupdate.html'
	model = SubAbout 
	form_class = SubAboutForm
	success_url = reverse_lazy('restroapp:adminhome')


#team
class A_TeamListView(AdminRequiredMixin,AdminBaseMixin,ListView):
	template_name = 'admintemplates/chef_teamlist.html'
	model = Team 
	context_object_name = 'chefteamlist'



class A_TeamDetailView(AdminRequiredMixin,AdminBaseMixin,DetailView):
	template_name = 'admintemplates/chef_detail.html'
	model = Team 
	context_object_name = 'chefdetail'


class A_TeamDeleteView(AdminRequiredMixin,AdminBaseMixin,DeleteView):
	template_name = 'admintemplates/chefdelete.html'
	model = Team 
	success_url = reverse_lazy('restroapp:a_teamlist')


class A_TeamCreateView(AdminRequiredMixin,AdminBaseMixin,CreateView):
	template_name = 'admintemplates/chefcreate.html'
	form_class = ChefForm 
	success_url = reverse_lazy('restroapp:a_teamlist')


class A_TeamUpadateView(AdminRequiredMixin,AdminBaseMixin,UpdateView):
	template_name = 'admintemplates/chefcreate.html'
	model = Team
	form_class = ChefForm
	success_url = reverse_lazy('restroapp:a_teamlist')


class A_ReasonCreateView(AdminRequiredMixin,AdminBaseMixin,CreateView):
	template_name = 'admintemplates/reasoncreate.html'
	form_class = ReasonToChooseForm
	success_url = reverse_lazy('restroapp:a_reasonlist')



class A_ReasonListView(AdminRequiredMixin,AdminBaseMixin,ListView):
	template_name = 'admintemplates/a_reasonlist.html'
	model = ReasonToChoose
	context_object_name = 'a_reasonlistview'


class A_ReasonDetailView(AdminRequiredMixin,AdminBaseMixin,DetailView):
	template_name = 'admintemplates/a_reasondetail.html'
	model = ReasonToChoose 
	context_object_name = 'a_reasondetailview'


class A_ReasonDeleteView(AdminRequiredMixin,AdminBaseMixin,DeleteView):
	template_name = 'admintemplates/a_reasondelete.html'
	model = ReasonToChoose
	success_url = reverse_lazy('restroapp:a_reasonlist')


class A_ReasonUpdateView(AdminRequiredMixin,AdminBaseMixin,UpdateView):
	template_name = 'admintemplates/reasoncreate.html'
	model = ReasonToChoose
	form_class = ReasonToChooseForm
	success_url = reverse_lazy('restroapp:a_reasonlist')



#our_special view
class A_SpecialCreateView(AdminRequiredMixin,AdminBaseMixin,CreateView):
	template_name = 'admintemplates/a_specialcreate.html'
	form_class = OurSpecialForm
	success_url = reverse_lazy('restroapp:a_speciallist')



class A_SpecialListView(AdminRequiredMixin,AdminBaseMixin,ListView):
	template_name = 'admintemplates/a_speciallist.html'
	model = OurSpecial
	context_object_name = 'a_speciallistview' 


class A_SpecialDetailView(AdminRequiredMixin,AdminBaseMixin,DetailView):
	template_name = 'admintemplates/a_specaildetail.html'
	model = OurSpecial
	context_object_name = 'a_specialdetailview'


class A_SpecialDeleteView(AdminRequiredMixin,AdminBaseMixin,DeleteView):
	template_name = 'admintemplates/a_specialdelete.html'
	model = OurSpecial
	success_url = reverse_lazy('restroapp:a_speciallist')


class A_SpecialUpdateView(AdminRequiredMixin,AdminBaseMixin,UpdateView):
	template_name = 'admintemplates/a_specialcreate.html'
	model = OurSpecial
	form_class = OurSpecialForm
	success_url = reverse_lazy('restroapp:a_speciallist')


#ClientVoice views
class A_VoiceCreateView(AdminRequiredMixin,AdminBaseMixin,CreateView):
	template_name = 'admintemplates/a_voicecreate.html'
	form_class = ClientVoiceForm
	success_url = reverse_lazy('restroapp:a_clientvoicelist')


class A_VoiceListView(AdminRequiredMixin,AdminBaseMixin,ListView):
	template_name = 'admintemplates/a_voicelist.html'
	model = ClientVoice
	context_object_name = 'a_voicelistview'


class A_VoiceDetailView(AdminRequiredMixin,AdminBaseMixin,DetailView):
	template_name = 'admintemplates/a_voicedetail.html'
	model = ClientVoice
	context_object_name = 'a_voicedetailview'


class A_VoiceDeleteView(AdminRequiredMixin,AdminBaseMixin,DeleteView):
	template_name = 'admintemplates/a_voicedelete.html'
	model = ClientVoice
	success_url = reverse_lazy('restroapp:a_clientvoicelist')


class A_VoiceUpdateView(AdminRequiredMixin,AdminBaseMixin,UpdateView):
	template_name = 'admintemplates/a_voicecreate.html'
	model = ClientVoice
	form_class = ClientVoiceForm
	success_url = reverse_lazy('restroapp:a_clientvoicelist')


#Order views
class A_OrderListView(AdminRequiredMixin,AdminBaseMixin,ListView):
	template_name = 'admintemplates/a_orderlist.html'
	queryset = Order.objects.all().order_by('-id')
	context_object_name = 'a_orderlistview'


class A_OrderDetailView(AdminRequiredMixin,AdminBaseMixin,DetailView):
	template_name = 'admintemplates/a_orderdetail.html'
	model = Order
	context_object_name = 'a_orderdetailview'


class A_OrderDeleteView(AdminRequiredMixin,AdminBaseMixin,DeleteView):
	template_name = 'admintemplates/a_orderdelete.html'
	model = Order
	success_url = reverse_lazy('restroapp:a_orderlist')



#banner
class A_BannerListView(AdminRequiredMixin,AdminBaseMixin,ListView):
	template_name = 'admintemplates/a_bannerlist.html'
	model = Banner 
	context_object_name = 'a_bannerlistview'


class A_BannerUpdateView(AdminRequiredMixin,AdminBaseMixin,UpdateView):
	template_name = 'admintemplates/a_bannerupdate.html'
	model = Banner
	form_class = BannerForm
	success_url = reverse_lazy('restroapp:adminhome')


#gallery
class A_GalleryCreateView(AdminRequiredMixin,AdminBaseMixin,CreateView):
	template_name = 'admintemplates/a_gallerycreate.html'
	form_class = GalleryForm
	success_url = reverse_lazy('restroapp:a_gallerylist')




class A_GalleryListView(AdminRequiredMixin,AdminBaseMixin,ListView):
	template_name = 'admintemplates/a_gallerylist.html'
	model = Gallery
	context_object_name = 'a_gallerylistview'


class A_GalleryDeleteView(AdminRequiredMixin,AdminBaseMixin,DeleteView):
	template_name = 'admintemplates/a_gallerydelete.html'
	model = Gallery
	success_url = reverse_lazy('restroapp:a_gallerylist')


class A_GalleryUpdateView(AdminRequiredMixin,AdminBaseMixin, UpdateView):
	template_name = 'admintemplates/a_gallerycreate.html'
	model = Gallery
	form_class = GalleryForm
	success_url = reverse_lazy('restroapp:a_gallerylist')



#food category views
class FoodCategoryCreateView(AdminRequiredMixin,AdminBaseMixin,CreateView):
	template_name = 'admintemplates/foodcategorycreate.html'
	form_class = FoodCategoryForm
	success_url = reverse_lazy('restroapp:foodcategorylist')

class FoodCategoryDeleteView(AdminRequiredMixin,AdminBaseMixin,DeleteView):
	template_name = 'admintemplates/foodcategorycdelete.html'
	model = FoodCategory 
	success_url = reverse_lazy('restroapp:foodcategorylist')

class FoodCategoryListView(AdminRequiredMixin,AdminBaseMixin,ListView):
	template_name = 'admintemplates/foodcategorylist.html'
	model = FoodCategory
	context_object_name = 'foodcategorylistview'



#food nemu views
class FoodMenuCreateView(AdminRequiredMixin,AdminBaseMixin,CreateView):
	template_name = 'admintemplates/foodmenucreate.html'
	form_class = FoodMenuForm
	success_url = reverse_lazy('restroapp:foodmenulist')


class FoodMenuUpdateView(AdminRequiredMixin,AdminBaseMixin,UpdateView):
	template_name = 'admintemplates/foodmenucreate.html'
	model = FoodMenu 
	form_class = FoodMenuForm
	success_url = reverse_lazy('restroapp:foodmenulist')


class FoodMenuListView(AdminRequiredMixin,AdminBaseMixin,ListView):
	template_name = 'admintemplates/foodmenulist.html'
	model = FoodMenu
	context_object_name = 'foodmenulistview'


class FoodMenuDetailView(AdminRequiredMixin,AdminBaseMixin,DetailView):
	template_name = 'admintemplates/foodmenudetail.html'
	model = FoodMenu 
	context_object_name = 'foodmenudetailview'


class FoodMenuDeleteView(AdminRequiredMixin,AdminBaseMixin,DeleteView):
	template_name = 'admintemplates/foodmenudelete.html'
	model = FoodMenu
	success_url = reverse_lazy('restroapp:foodmenulist')



#admin authentication
class PasswordChangeView(AdminRequiredMixin,AdminBaseMixin,TemplateView):
	template_name = 'admintemplates/change-password.html'


class PasswordChangeDoneView(AdminRequiredMixin,AdminBaseMixin,TemplateView):
	template_name = 'admintemplates/password_change_done.html'



class A_SettingsView(AdminRequiredMixin,AdminBaseMixin,TemplateView):
	template_name = 'admintemplates/a_settings.html'

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		user = self.request.user
		thisuser = Admin.objects.filter(user=user)
		context["admindetail"]=thisuser

		return context


#admin registration view
class A_RegistrationView(AdminRequiredMixin,AdminBaseMixin,CreateView):
	template_name = 'admintemplates/a_registration.html'
	form_class = AdminRegisterForm
	success_url = reverse_lazy('restroapp:adminhome')


	def form_valid(self,form):
		uname = form.cleaned_data['username']
		pword = form.cleaned_data['password']

		user = User.objects.create_user(uname, '', pword)
		form.instance.user = user

		return super().form_valid(form)




	def signup(request):
	    # print(form.errors.as_data())
		if form.is_valid():
			user = form.save(commit=False)
			user.is_active = False
			user.save()
			current_site = get_current_site(request)
			mail_subject = 'Activate your account.'
			message = render_to_string('admintemplates/password_reset_email.html', {
	            'user': user,
	            'domain': current_site.domain,
	            'uid': urlsafe_base64_encode(force_bytes(user.pk)),
	            'token': default_token_generator.make_token(user),
	        })
			to_email = form.cleaned_data.get('email')
			email = EmailMessage(
	            mail_subject, message, to=[to_email]
	        )
			email.send()
			return HttpResponse('Please confirm your email address to complete the registration')



def activate(request, uidb64, token):
	try:
		uid = urlsafe_base64_decode(uidb64).decode()
		user = UserModel._default_manager.get(pk=uid)
	except(TypeError, ValueError, OverflowError, User.DoesNotExist):
		user = None
	if user is not None and default_token_generator.check_token(user, token):
		user.is_active = True
		user.save()
		return HttpResponse('Thank you for your email confirmation. Now you can login your account.')
	else:
		return HttpResponse('Activation link is invalid!')

