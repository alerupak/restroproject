from django import template

register = template.Library()

from ..models import Message


@register.filter
def unread_messages(user):
    return user.messages_set.filter(read=False).count()
    #replace the messages_set with the appropriate related_name, and also the filter field. (I am assuming it to be "read")

register.simple_tag(unread_messages)
