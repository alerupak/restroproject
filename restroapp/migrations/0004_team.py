# Generated by Django 3.0.8 on 2020-07-26 12:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('restroapp', '0003_subabout'),
    ]

    operations = [
        migrations.CreateModel(
            name='Team',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('post', models.CharField(max_length=50)),
                ('image', models.ImageField(upload_to='team')),
            ],
        ),
    ]
