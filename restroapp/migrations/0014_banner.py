# Generated by Django 3.0.8 on 2020-07-29 16:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('restroapp', '0013_auto_20200729_2202'),
    ]

    operations = [
        migrations.CreateModel(
            name='Banner',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('slogon', models.CharField(max_length=100)),
                ('some_text', models.CharField(max_length=200)),
                ('image', models.ImageField(upload_to='banner')),
                ('price', models.CharField(blank=True, max_length=20, null=True)),
            ],
        ),
    ]
