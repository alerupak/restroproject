from django.db import models
from django.contrib.auth.models import User,Group
# Create your models here.

class TimeStamp(models.Model):
	created_at = models.DateTimeField(auto_now_add = True, null = True, blank = True)
	updated_at = models.DateTimeField(auto_now = True, null = True, blank = True)

	class Meta:
		abstract = True


class Admin(TimeStamp):
	user = models.OneToOneField(User, on_delete = models.CASCADE)
	image = models.ImageField(upload_to = 'admin', null = True, blank = True)
	full_name = models.CharField(max_length = 100)
	email = models.EmailField()
	phone = models.CharField(max_length = 30)
	post = models.CharField(max_length = 30)
	address = models.CharField(max_length = 50)

	def save(self, *args,**kwargs):
		group,created = Group.objects.get_or_create(name = 'Admin')
		self.user.groups.add(group)

		super().save(*args,**kwargs)


	def __str__(self):
		return self.full_name




class Contact(models.Model):
	address = models.CharField(max_length = 200)
	phone = models.CharField(max_length = 50)
	email = models.EmailField()
	open_day = models.CharField(max_length = 50)
	open_time = models.CharField(max_length = 50)


class About(models.Model):
	title = models.CharField(max_length = 400)
	detail = models.TextField()


class SubAbout(models.Model):
	title = models.CharField(max_length = 400)
	detail = models.TextField()



class Message(TimeStamp,models.Model):
	name = models.CharField(max_length = 100)
	email = models.EmailField()
	message = models.TextField()


class Team(TimeStamp,models.Model):
	name = models.CharField(max_length = 100)
	post = models.CharField(max_length = 50)
	image = models.ImageField(upload_to = 'team')
	phone = models.CharField(max_length = 30, null = True, blank = True)
	email = models.EmailField(null = True, blank = True)
	facebook = models.CharField(max_length = 100, null = True, blank = True)
	twitter = models.CharField(max_length = 100, null = True, blank = True)
	address = models.CharField(max_length = 100, null = True, blank = True)




class ReasonToChoose(models.Model):
	title = models.CharField(max_length = 100)
	few_line = models.CharField(max_length = 500)
	image = models.ImageField(upload_to = 'reason' , null = True, blank = True)


class ClientVoice(models.Model):
	name = models.CharField(max_length = 100)
	voice = models.TextField()
	post_company = models.CharField(max_length = 100)
	image = models.ImageField(upload_to = 'clientvoice') 


class Order(TimeStamp,models.Model):
	name = models.CharField(max_length = 100)
	order_name = models.TextField()
	phone = models.CharField(max_length = 50)
	location = models.CharField(max_length = 100)
	pickup_time = models.CharField(max_length = 100)



class OurSpecial(models.Model):
	food_name = models.CharField(max_length = 100)
	combo = models.CharField(max_length = 100)
	price = models.CharField(max_length = 20)
	image = models.ImageField(upload_to = 'special')


class Banner(models.Model):
	slogon = models.CharField(max_length = 100)
	some_text = models.CharField(max_length = 200)
	image = models.ImageField(upload_to = 'banner')
	price = models.CharField(max_length = 20, null = True, blank = True)


class Gallery(models.Model):
	image = models.ImageField(upload_to = 'gallery')
	title = models.CharField(max_length = 100, null = True, blank = True)


class FoodCategory(TimeStamp):
	food_type = models.CharField(max_length = 50)

	def __str__(self):
		return self.food_type


class FoodMenu(TimeStamp,models.Model):
	food_name = models.CharField(max_length = 50)
	price = models.CharField(max_length = 50)
	category = models.ForeignKey(FoodCategory, on_delete = models.CASCADE)
	extra = models.CharField(max_length = 200, null = True, blank = True)
