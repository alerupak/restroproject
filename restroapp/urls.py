from django.urls import path, include
from .views import *
from restroapp import views


from django.contrib.auth import views as auth_views

app_name = 'restroapp'
urlpatterns = [



#client url
path('',ClientHomeView.as_view(), name = "clienthome"),



#about
path('about/<int:pk>/', AboutDetailView.as_view(), name = 'aboutdetail'),
path('about/<int:pk>/', SubAboutDetailView.as_view(), name = 'subabout'),


#message
path('contact/', MessageCreateView.as_view(), name = 'messagecreate'),


#contact
path('contact/', ContactListView.as_view(), name = 'contactlist'),


#team
path('about/<int:pk>/', TeamListView.as_view(), name = 'teamlist'),

#reason_to_choose
path('about/<int:pk>/', ReasonToChooseListView.as_view(), name = 'reason_to_choose'),

#client_voice
path('', ClientVoiceListView.as_view(), name = 'clientvoice'),


#order
path('order/', OrderCreateView.as_view(), name = 'order'),


#our_special
path('', OurSpecialListView.as_view(), name = 'ourspecial'),

#banner
path('', BannerDetailView.as_view(), name = 'bannerdetail'),


#gallery
path('gallery/list/', GalleryListView.as_view(), name = 'gallerylist'),
#food menu
path('menu/', Food_MenuListView.as_view(), name = 'menulist'),

#category
path('menu/', FoodCategory_ListView.as_view(), name = 'foodcategory'),



#login/logout
path('admin-login/',LoginView.as_view(),name="login"),
path('logout/', LogoutView.as_view(), name = 'logout'),



#admin urls start here

path('admin/home/', AdminHomeView.as_view(), name = 'adminhome'),

#Admin Profile
path('admin/profile/detail', AdminProfileDetailView.as_view(), name = 'adminprofiledetail'),


#contact 
path('admin/contact/', A_ContectListView.as_view(), name = 'a_contactlist'),
path('admin/contact/create/', A_ContactCreateView.as_view(), name = 'a_contactcreate'),
path('admin/contact/<int:pk>/update/', AdminContactUpdateView.as_view(), name = 'contactupdate'),


#message
path('message/list/', MessageListView.as_view(), name = 'messagelist'),
path('message/<int:pk>/delete/', MessageDeleteView.as_view(), name = 'messagedelete'),
path('message/<int:pk>/detail/', MessageDetailView.as_view(), name = 'messagedetailview'),



#About
path('admin/about/list/', A_AboutListView.as_view(), name = 'a_aboutlist'),
path('about/<int:pk>/update/', AboutUpdateView.as_view(), name = 'aboutupdateview'),
path('admin/subabout/list/', A_SubAboutListView.as_view(), name = 'a_subaboutlist'),
path('subabout/<int:pk>/update/', SubAboutUpdateView.as_view(), name = 'subaboutupdate'),

#team
path('admin/team/create/', A_TeamCreateView.as_view(), name = 'a_teamcreate'),
path('admin/team/list/', A_TeamListView.as_view(), name = 'a_teamlist'),
path('admin/team/<int:pk>/detail/', A_TeamDetailView.as_view(), name = 'a_teamdetail'),
path('admin/team/<int:pk>/delete/', A_TeamDeleteView.as_view(), name = 'a_teamdelete'),
path('admin/team/<int:pk>/update', A_TeamUpadateView.as_view(), name = 'a_teamupdate'),


#reason to choose us
path('admin/reason/create/', A_ReasonCreateView.as_view(), name = 'a_reasoncreate'),
path('admin/reason/list/', A_ReasonListView.as_view(), name = 'a_reasonlist'),
path('admin/reason/<int:pk>/detail/', A_ReasonDetailView.as_view(), name = 'a_reasondetail'),
path('admin/reason/<int:pk>/delete/', A_ReasonDeleteView.as_view(), name = 'a_reasondelete'),
path('admin/reason/<int:pk>/update/', A_ReasonUpdateView.as_view(), name = 'a_reasonupdate'),



#our special
path('admin/special/create/', A_SpecialCreateView.as_view(), name = 'a_specialcreate'),
path('admin/special/list/', A_SpecialListView.as_view(), name = 'a_speciallist'),
path('admin/special/<int:pk>/detail/', A_SpecialDetailView.as_view(), name = 'a_specialdetail'),
path('admin/special/<int:pk>/delete/', A_SpecialDeleteView.as_view(), name = 'a_specialdelete'),
path('admin/special/<int:pk>/update/', A_SpecialUpdateView.as_view(), name = 'a_specialupdate'),



#clientVoice urls
path('admin/voice/create/', A_VoiceCreateView.as_view(), name = 'a_clientvoicecreate'),
path('admin/voice/list/', A_VoiceListView.as_view(), name = 'a_clientvoicelist'),
path('admin/voice/<int:pk>/detail/', A_VoiceDetailView.as_view(), name = 'a_clientvoicedetail'),
path('admin/voice/<int:pk>/delete/', A_VoiceDeleteView.as_view(), name = 'a_clientvoicedelete'),
path('admin/voice/<int:pk>/update/', A_VoiceUpdateView.as_view(), name = 'a_clientvoiceupdate'), 



#online order urls
path('admin/order/list/', A_OrderListView.as_view(), name = 'a_orderlist'),
path('admin/order/<int:pk>/detail/', A_OrderDetailView.as_view(), name = 'a_orderdetail'),
path('admin/order/<int:pk>/delete/', A_OrderDeleteView.as_view(), name = 'a_orderdelete'),


#banner urls
path('admin/banner/list/', A_BannerListView.as_view(), name = 'a_bannerlist'),
path('admin/banner/<int:pk>/update/', A_BannerUpdateView.as_view(), name = 'a_bannerupdate'),


#gallery urls
path('admin/gallery/create/', A_GalleryCreateView.as_view(), name = 'a_gallerycreate'),
path('admin/gallery/list/', A_GalleryListView.as_view(), name = 'a_gallerylist'),
path('admin/gallery/<int:pk>/delete/', A_GalleryDeleteView.as_view(), name = 'a_gallerydelete'),
path('admin/gallery/<int:pk>/update/', A_GalleryUpdateView.as_view(), name = 'a_galleryupdate'),


#FoodCategory urls
path('admin/food-category/create/', FoodCategoryCreateView.as_view(), name = 'foodcategorycreate'),
path('admin/food-category/<int:pk>/delete/',FoodCategoryDeleteView.as_view(), name = 'foodcategorydelete'),
path('admin/food-category/list/', FoodCategoryListView.as_view(), name = 'foodcategorylist'),


#FoodMenu urls
path('admin/food-menu/create/', FoodMenuCreateView.as_view(), name = 'foodmenucreate'),
path('admin/food-menu/<int:pk>/update', FoodMenuUpdateView.as_view(), name = 'foodmenuupdate'),
path('admin/food-menu/list/', FoodMenuListView.as_view(), name = 'foodmenulist'),
path('admin/food-menu/<int:pk>/', FoodMenuDetailView.as_view(), name = 'foodmenudetail'),
path('admin/food-menu/<int:pk>/delete/', FoodMenuDeleteView.as_view(), name = 'foodmenudelete'),


#admin settings
path('admin/settings',A_SettingsView.as_view(), name = 'a_settings'),


#admin registration
path('admin/registration/',A_RegistrationView.as_view(), name = 'a_registration'),
path('activate/<uidb64>/<token>/',views.activate, name='activate'),

#username check
path('username-check/',UsernameCheckView.as_view(), name = 'username_check'),



#user authentication
path(
        'change-password/',
        auth_views.PasswordChangeView.as_view(template_name='admintemplates/change-password.html',
        	success_url = '/change-password-done/'),
        name = 'password_change'
    ),
path(
		'change-password-done/',
		auth_views.PasswordChangeDoneView.as_view(
			template_name = 'admintemplates/password_change_done.html'),
		name = 'password_change_done' 
	),


path('password-reset/',auth_views.PasswordResetView.as_view(
    template_name = 'admintemplates/password_reset_form.html',
    subject_template_name='admintemplates/password_reset_subject.txt',
    email_template_name='admintemplates/password_reset_email.html',
    success_url = '/password_reset/done/'
 ), name = 'password_reset_form'),


path('password_reset/done/',
     auth_views.PasswordResetDoneView.as_view(
         template_name='admintemplates/password_reset_done.html'
     ),
     name='password_reset_done'),
path('password-reset-confirm/<uidb64>/<token>/',
     auth_views.PasswordResetConfirmView.as_view(
         template_name='admintemplates/password_reset_confirm.html',
         success_url = '/reset/done/'
     ),
     name='password_reset_confirm'),
path('reset/done/',
     auth_views.PasswordResetCompleteView.as_view(
         template_name='admintemplates/password_reset_complete.html'
     ),
     name='password_reset_complete'),
]
